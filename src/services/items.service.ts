import { BaseItem, Item } from '../models/item.interface';
import { Items } from '../models/items.interface';
const db = require('../../pg-providers');

/**
 * In-Memory Store
 * to be replaced with real database
 */
let items: Items = {
  1: {
    id: 1,
    name: 'Burger',
    price: 599,
    description: 'Tasty burger',
    image: 'https://cdn.auth0.com/blog/whatabyte/burger-sm.png',
  },
  2: {
    id: 2,
    name: 'Pizza',
    price: 299,
    description: 'Cheesy pizza',
    image: 'https://cdn.auth0.com/blog/whatabyte/pizza-sm.png',
  },
  3: {
    id: 3,
    name: 'Tea',
    price: 199,
    description: 'Informative',
    image: 'https://cdn.auth0.com/blog/whatabyte/tea-sm.png',
  },
};

/**
 * Service methods
 */
export const findAll = async (): Promise<Item[]> => Object.values(items);

export const findAllItems = async () => {
  let results = await db.query(`SELECT * FROM clients`).catch(console.log);
  return results;
};

export const find = async (id: number): Promise<Item> => items[id];

export const create = async (newItem: BaseItem): Promise<Item> => {
  const id = new Date().valueOf();

  items[id] = {
    id,
    ...newItem,
  };

  return items[id];
};

export const update = async (
  id: number,
  itemUpdate: BaseItem
): Promise<Item | null> => {
  const item = await find(id);

  if (!item) {
    return null;
  }

  items[id] = { id, ...itemUpdate };

  return items[id];
};

export const remove = async (id: number): Promise<null | void> => {
  const item = await find(id);

  if (!item) {
    return null;
  }

  delete items[id];
};
