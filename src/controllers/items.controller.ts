import { Request, Response } from 'express';
import * as ItemService from '../services/items.service';

exports.index = async function (req: Request, res: Response) {
  try {
    const result = await ItemService.findAllItems();
    res.status(200).send(result.rows);
  } catch (e: any) {
    res.status(500).send(e.message);
  }
};
