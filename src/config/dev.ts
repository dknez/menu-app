module.exports = {
  database: {
    user: 'postgres',
    host: 'localhost',
    db_name: 'kibs',
    password: 'mysecretpassword',
    port: 5432,
  },
};
