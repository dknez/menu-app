const appConfig = require('./src/config/config');

const Pool = require('pg').Pool;
const pool = new Pool({
  user: appConfig.database.user,
  password: appConfig.database.password,
  host: appConfig.database.host,
  database: appConfig.database.db_name,
  port: 5432,
});

module.exports = pool;
