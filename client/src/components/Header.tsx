import axios from 'axios';
import { response } from 'express';
import React, { Component } from 'react';
class Header extends Component {
  callBackendAPI = async () => {
    const response = await axios.get('/api/menu/items');

    if (response.status !== 200) {
      throw Error();
    }
    return response.data;
  };

  render() {
    return (
      <div>
        <button onClick={this.callBackendAPI}>Call backend API</button>
      </div>
    );
  }
}
export default Header;
